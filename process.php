<?php
$errors = array(); // array to hold validation errors
$data = array(); // array to pass back data
// validate the variables ======================================================
if (empty($_POST['name']))
$errors['name'] = 'Name is required.';
if (empty($_POST['email']))
$errors['email'] = 'Email is required.';
if (empty($_POST['message']))
$errors['message'] = 'Message is required.';


if (empty($errors) == false) {
  $data['success'] = false;
  $data['errors'] = $errors;
  $data['messageError'] = 'Please check for missing fields';
} else {
  $data['success'] = true;
  $data['messageSuccess'] = 'Form has been sent';
  
  
  
}
// return all our data to an AJAX call
echo json_encode($data);