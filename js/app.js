var app = angular.module('rApp', ['ngRoute', 'ngAnimate']);

app.config(function($routeProvider){
	
	$routeProvider
     .when('/', {
           templateUrl : 'home.html',
           controller :    'homeController'
      })
      .when('/contact', {
           templateUrl: 'contact.html',
           controller: 'contactController'
      });

    
});