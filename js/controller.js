
app.controller("homeController", function ($scope, buttonFactory, newService) {
    $scope.myClass = 'homepage';

    newService.getContent(function(data) {
        $scope.page = data[0];
    })
	
	
    $scope.buttons = buttonFactory.getButtons();
});

app.controller("contactController", function ($scope, newService, formSubmit) {
    $scope.myClass = 'contactpage';
    newService.getContent(function (data) {
        console.log(data);
        $scope.page = data[2];
    });
	
	// submission message doesn't show when page loads
    $scope.submission = false;
    $scope.formData = {};
    $submitMessage = "Not Submitted"
    $scope.submit = function() {
        formSubmit.Submit($scope.formData, function (data) {
            $scope.submitMessage = data.messageSuccess;
            $scope.submission = true;
            if (!data.success) {
                // if not successful, bind errors to error variables
                $scope.submissionMessage = data.messageError;
            } else {
                // if successful, bind success message to message
                $scope.submissionMessage = data.messageSuccess;
                $scope.formData = {}; // form fields are emptied with this line
            }
        });
    }
})

//Factory
app.factory('buttonFactory', function(){
    var _buttons = [
        { text: "Turkey", active: true },
        { text: "Duck", active: true },
        { text: "Chicken", active: true },
    ];
    var myService = {};

    myService.getButtons = function(){
        return _buttons;
    }

    return myService;
});

//Service
app.service('newService', function ($http) {
    this.getContent = function (callback) {
        $http.get('mock/content.JSON')
        //set up call back function to be defined in the controller
		.success(callback);
    }
});


app.service('formSubmit', function ($http, $httpParamSerializer) {

    this.Submit = function (formData, callback) {       
        $http({
            method: 'POST',
            url: 'process.php',
            data: $httpParamSerializer(formData), // pass in data as strings
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .success(callback);
    };
});



app.filter('makeLower', function () {
    return function (item) {
        return item.toLowerCase();
    };
});

app.filter('startsWithLetter', function () {
    return function (items, letter) {
        //Array to Return
        var results = [];
        var testPhrase = new RegExp(letter, 'i');

        for (var i = 0; i < items.length; i++) {
            if (testPhrase.test(items[i].text.substring(0,1))) {
                results.push(items[i]);
            }
        }
        return results;
    };
});